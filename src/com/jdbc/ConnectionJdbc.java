package com.jdbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class ConnectionJdbc {
    private static final String URL = "jdbc:mysql://localhost:3306/CCO_bd"; // Cambia "tu_base_de_datos" por el nombre de tu base de datos.
    private static final String USUARIO = "root"; // nombre de usuario de MySQL.
    private static final String CONTRASENA = "123456*.*"; // contraseña de MySQL.

    public static Connection obtenerConexion() {
        Connection conexion = null;

        try {
            // Cargar el controlador de MySQL.
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Establecer la conexión a la base de datos.
            conexion = DriverManager.getConnection(URL, USUARIO, CONTRASENA);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Error: No se pudo cargar el controlador de MySQL.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error: No se pudo establecer la conexión a la base de datos.");
        }

        return conexion;
    }
    public static void cerrarConexion(Connection conexion) {
        if (conexion != null) {
            try {
                conexion.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al cerrar la conexión a la base de datos.");
            }
        }
    }
}