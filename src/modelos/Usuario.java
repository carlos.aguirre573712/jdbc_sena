package modelos;
import com.jdbc.ConnectionJdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class Usuario {
    private int id; // Puedes agregar un campo ID si deseas un identificador único para cada usuario.
    private String nombres;
    private String apellidos;
    private String rol;
    private String celular;
    private String email;
    private String cedula;
    private String password;

    public Usuario(String nombres, String apellidos, String rol, String celular, String email, String cedula, String password) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.rol = rol;
        this.celular = celular;
        this.email = email;
        this.cedula = cedula;
        this.password = password;
    }

    // Insertar un nuevo usuario en la base de datos.
    public void insertarUsuario() {
        Connection conexion = ConnectionJdbc.obtenerConexion();

        if (conexion != null) {
            String query = "INSERT INTO usuario (nombres, apellidos, rol, celular, email, cedula, password) VALUES (?, ?, ?, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = conexion.prepareStatement(query)) {
                preparedStatement.setString(1, this.nombres);
                preparedStatement.setString(2, this.apellidos);
                preparedStatement.setString(3, this.rol);
                preparedStatement.setString(4, this.celular);
                preparedStatement.setString(5, this.email);
                preparedStatement.setString(6, this.cedula);
                preparedStatement.setString(7, this.password);

                preparedStatement.executeUpdate();
                System.out.println("Usuario insertado correctamente.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al insertar el usuario en la base de datos.");
            } finally {
                ConnectionJdbc.cerrarConexion(conexion);
            }
        }
    }

    // Consultar un usuario por su ID o algún otro criterio.
    public static Usuario consultarUsuarioId(int id) {
        Connection conexion = ConnectionJdbc.obtenerConexion();
        Usuario usuario = null;

        if (conexion != null) {
            String query = "SELECT * FROM usuario WHERE id_Usuario = ?";

            try (PreparedStatement preparedStatement = conexion.prepareStatement(query)) {
                preparedStatement.setInt(1, id);

                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    usuario = new Usuario(
                            resultSet.getString("nombres"),
                            resultSet.getString("apellidos"),
                            resultSet.getString("rol"),
                            resultSet.getString("celular"),
                            resultSet.getString("email"),
                            resultSet.getString("cedula"),
                            resultSet.getString("password")
                    );
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al consultar el usuario en la base de datos.");
            } finally {
                ConnectionJdbc.cerrarConexion(conexion);
            }
        }

        return usuario;
    }

    // Método para consultar un usuario por su cédula
    public static Usuario consultarUsuario(String cedula) {
        Connection conexion = ConnectionJdbc.obtenerConexion();
        Usuario usuario = null;
        try {
            String query = "SELECT * FROM usuario WHERE cedula = ?";
            try (PreparedStatement statement = conexion.prepareStatement(query)) {
                statement.setString(1, cedula);

                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        usuario = new Usuario(
                                resultSet.getString("nombres"),
                                resultSet.getString("apellidos"),
                                resultSet.getString("rol"),
                                resultSet.getString("celular"),
                                resultSet.getString("email"),
                                resultSet.getString("cedula"),
                                resultSet.getString("password")
                        );
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Error al consultar usuario en la base de datos.");
        }

        return usuario;
    }


    // Actualizar un usuario en la base de datos.
    public void actualizarUsuario(String cedula) {
        Connection conexion = ConnectionJdbc.obtenerConexion();

        if (conexion != null) {
            String query = "UPDATE usuario SET nombres = ?, apellidos = ?, rol = ?, celular = ?, email = ?, password = ? WHERE cedula = ?";

            try (PreparedStatement preparedStatement = conexion.prepareStatement(query)) {
                preparedStatement.setString(1, this.nombres);
                preparedStatement.setString(2, this.apellidos);
                preparedStatement.setString(3, this.rol);
                preparedStatement.setString(4, this.celular);
                preparedStatement.setString(5, this.email);
               // preparedStatement.setString(6, this.cedula);
                preparedStatement.setString(6, this.password);
                preparedStatement.setString(7, this.cedula);

                int filasActualizadas = preparedStatement.executeUpdate();
                if (filasActualizadas > 0) {
                    System.out.println("Usuario actualizado correctamente.");
                } else {
                    System.err.println("No se encontró el usuario a actualizar.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al actualizar el usuario en la base de datos.");
            } finally {
                ConnectionJdbc.cerrarConexion(conexion);
            }
        }
    }
    // Eliminar un usuario de la base de datos.
    public static Usuario eliminarUsuario(String cedula) {
        Connection conexion = ConnectionJdbc.obtenerConexion();

        if (conexion != null) {
            try {
                String query = "DELETE FROM usuario WHERE cedula=?";
                try (PreparedStatement statement = conexion.prepareStatement(query)) {
                    statement.setString(1, cedula);

                    statement.executeUpdate();
                    System.out.println("Usuario eliminado correctamente.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error al eliminar usuario de la base de datos.");
            } finally {
                ConnectionJdbc.cerrarConexion(conexion);
            }
        }
        return null;
    }



    // Getters y setters para los campos de la clase.
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
