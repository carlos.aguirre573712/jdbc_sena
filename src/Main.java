// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import com.jdbc.ConnectionJdbc;
import modelos.Usuario;

import java.sql.*;

public class Main {
    public static void main(String[] args) {

        Connection conexion = ConnectionJdbc.obtenerConexion();

        if (conexion != null) {
            try {
                // Realiza operaciones en la base de datos aquí.
                Statement statement = conexion.createStatement();
                ResultSet resultSet;
                // Ejecuta consultas, actualizaciones, etc.
                // ...

                // Crear usuario
                // statement.executeUpdate()

                    /*Usuario nuevoUsuario = new Usuario("Carlos", "Aguirre", "1", "1234567890", "carlos@cco.com", "123456789", "123456");
                nuevoUsuario.insertarUsuario();*/

                /*Usuario nuevoUsuario = new Usuario("Oscar", "Montoya", "1", "1234567890", "oscar@cco.com", "234567890", "123456");
                nuevoUsuario.insertarUsuario();*/

               Usuario nuevoUsuario = new Usuario("Yineth", "Peña", "1", "1234567890", "yineth@cco.com", "345678901", "123456");
                nuevoUsuario.insertarUsuario();

                // Consultar usuario por id 3
                Usuario usuario = Usuario.consultarUsuarioId(1);

                if (usuario != null) {
                    System.out.println("Usuario consultado: " + usuario.getNombres() + " " + usuario.getApellidos() + ", "  + "Cédula: "+ usuario.getCedula() + ", Email: "  + usuario.getEmail()+ ", "  + "Celular: "+ usuario.getCelular());
                }

                // Consultar un usuario por su cédula
                Usuario usuarioCedula = Usuario.consultarUsuario("345678901");
                if (usuarioCedula != null) {
                    System.out.println("Usuario consultado: " + usuarioCedula.getNombres() + " " + usuarioCedula.getApellidos() + ", "  + "Cédula: "+ usuarioCedula.getCedula() + ", Email: "  + usuarioCedula.getEmail()+ ", "  + "Celular: "+ usuarioCedula.getCelular());;
                }


                // Actualizar Usuario
                Usuario updateUsuario = Usuario.consultarUsuario("345678901");
                if (updateUsuario != null) {
                    updateUsuario.setNombres("Yineth");
                    updateUsuario.setApellidos("Peña");
                    updateUsuario.actualizarUsuario("345678901");
                }


                // Eliminar un usuario por su cédula
                Usuario deleteUser = Usuario.eliminarUsuario("345678901");

                //Consultar todos los roles

               resultSet = statement.executeQuery("SELECT * FROM usuario");
               resultSet.next();
               do {
                   System.out.println(resultSet.getInt("id_Usuario") + ": "+ resultSet.getString("Nombres"));
               }while (resultSet.next());

                // Cierra la conexión cuando hayas terminado.
                ConnectionJdbc.cerrarConexion(conexion);
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Error en la operación con la base de datos.");
            }
        }

    }
}